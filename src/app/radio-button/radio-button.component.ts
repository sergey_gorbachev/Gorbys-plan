import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {Subscription} from 'rxjs';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss']
})
export class RadioButtonComponent implements OnInit {

  private subscription: Subscription;
  private subscriptionValidate: Subscription;
  value: string;

  firstVal: string;
  secondVal: string;
  thirdVal: string;
  forthVal: string;
  sazba: number;
  length: number;

  disabled1: boolean;
  disabled2: boolean;
  disabled3: boolean;
  disabled4: boolean;

  constructor(public appService: AppService, public messageService: MessageService) {
    this.firstVal = '1';
    this.secondVal = '3';
    this.thirdVal = '5';
    this.forthVal = '7';

    this.value = appService.fixedPeriodInputValue.toString();
    this.sazba = this.appService.setSazba(appService.fixedPeriodInputValue);
    // this.sazba = this.appServise.setSazba(this.value)
    this.subscription = this.appService.observableLength
      .subscribe(item => {
        this.length = item;
        this.disabled1 = +this.firstVal > item;
        this.disabled2 = +this.secondVal > item;
        this.disabled3 = +this.thirdVal > item;
        this.disabled4 = +this.forthVal > item;
      });

    this.subscriptionValidate = this.appService.validateCallSource
      .subscribe(() => {
        appService.isFixedPeriodValid = this.validate();
      });
  }

  ngOnInit() {
  }

  handleClick( ) {
    this.sazba = this.appService.setSazba(+this.value);
    console.log(this.sazba * 100);
    // + operator converts string to number
    this.appService.fixedPeriodInputValue = +this.value;
  }

  validate() {
    if (+this.value > this.length) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Fixační doba',
        detail: 'Nesmí přesáhnout délku hypotéky',
        life: 5000
      });
      return false;
    } else {
      return true;
    }
  }
}
