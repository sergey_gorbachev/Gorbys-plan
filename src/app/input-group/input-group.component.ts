import {Component, ElementRef, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-input-group',
  templateUrl: './input-group.component.html',
  styleUrls: ['./input-group.component.scss']
})
export class InputGroupComponent implements OnInit {
  @Input()
  textInput: string;
  @Input()
  valueInput: string;
  @Input()
  numberInput: number;
  @Input()
  renderSpan: boolean;

  @Input()
  label: boolean;
  constructor() {
  }

  ngOnInit() {
  }

}
