import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  // the first slider
  private _firstValue: number;
  private _secondValue: number;
  private _actualValue: number;
  private _stepValue: number;

  constructor() { }

  get firstValue(): number {
    return this._firstValue;
  }

  set firstValue(value: number) {
    this._firstValue = value;
  }

  get secondValue(): number {
    return this._secondValue;
  }

  set secondValue(value: number) {
    this._secondValue = value;
  }

  get actualValue(): number {
    return this._actualValue;
  }

  set actualValue(value: number) {
    this._actualValue = value;
  }
  get stepValue(): number {
    return this._stepValue;
  }

  set stepValue(value: number) {
    this._stepValue = value;
  }
}
