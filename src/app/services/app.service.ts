// tslint:disable:variable-name
import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {MessageService} from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public data;
  public dataChart: any;
  public observableLength;
  // Observable string sources
  public validateCallSource = new Subject<any>();
  private _propertyCostSliderValue: number;
  private _propertyCostInputValue: number;
  private _loanSliderValue: number;
  private _loanInputValue: number;
  private _lengthSliderValue: number;
  private _salarySliderValue: number;
  private _salaryInputValue: number;
  private _rate: number;
  rpsnRate = 0.03;
  private interestCost: number;


  private _isPropertyCostValid: boolean;
  private _isLoanValid: boolean;
  private _islengthValid: boolean;
  private _isSalaryValid: boolean;
  private _isFixedPeriodValid: boolean;
  private _isInstallmentValid: boolean;

  private _fixedPeriodInputValue: number;
  // private _setSazba: number;
  //
  private firstVal = 1;
  private secondVal = 3;
  private thirdVal = 5;
  private forthVal = 7;
  // sazba: string;
  // value: string;
  //
  private firstSazba = 0.0379;
  private secondSazba = 0.0339;
  private thirdSazba = 0.0319;
  private forthSazba = 0.0279;

  constructor(public messageService: MessageService) {

    this.observableLength = new BehaviorSubject<number>(this.lengthSliderValue);

    this.propertyCostSliderValue = 300000;
    this.loanSliderValue = 200000;
    this.lengthSliderValue = 7;
    this.salarySliderValue = 30000;
    this.fixedPeriodInputValue = 5;

    this.propertyCostInputValue = null;
    this.loanInputValue = null;
    this.salaryInputValue = null;

    this.data = this.createSliderData();
    this.dataChart = this.createChartData();
  }

  public calculateInstallment() {
    // D = loan
    const loan = this.loanInputValue !== null ? this.loanInputValue : this.loanSliderValue;
    // n = length
    const length = this.lengthSliderValue;
    // i = rate

    /*    // v = discFactor
        const discFactor = 1 / (1 + rate);

        const numerator = loan * rate;
        const denominator = 1 - Math.pow(discFactor, length * 12);
        console.log(loan, length, rate, discFactor);

        const annuityInstallment = numerator / denominator;*/


    const loanPerYear = loan / length;
    const interestRate = loanPerYear * this.rate;
    const rpsn = loanPerYear * this.rpsnRate;

    return (loanPerYear + interestRate + rpsn) / 12;
  }

  public calculateMortgageCost() {
    const loan = this.loanInputValue !== null ? this.loanInputValue : this.loanSliderValue;
    const rpsn = loan * this.rpsnRate;
    const interestRate = loan * this.rate;
    return loan + rpsn + interestRate;
  }

  public calculateCountOfInstallment() {
    return this.lengthSliderValue * 12;
  }

  public calculateInterest(mortgageCost: number) {
    const loan = this.loanInputValue !== null ? this.loanInputValue : this.loanSliderValue;
    this.interestCost = mortgageCost - loan;
    return mortgageCost - loan;

  }

  // Service message commands
  validate() {
    this.validateCallSource.next();
    if (this.isLoanValid && this.islengthValid) {
      const installment = this.calculateInstallment();
      this.isInstallmentValid = this.validateInstallment(installment);
    }
    return this.islengthValid && this.isFixedPeriodValid && this.isLoanValid && this.isPropertyCostValid
      && this.isSalaryValid && this.isInstallmentValid;
  }

  validateInstallment(installment: number) {
    const salary = this.salaryInputValue !== null ? this.salaryInputValue : this.salarySliderValue;
    console.log(installment, salary * 0.45);
    if (installment !== null && salary !== 0 && installment > salary * 0.45) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Riziková hypotéka',
        detail: 'Jednotlivá splátka nesmí přesáhnout 45% výši měsíčního příjmu',
        life: 5000
      });
      return false;
    } else {
      return true;
    }
  }

  public createSliderData() {
    return {
      propertyCost: {
        min: 200000,
        max: 10000000,
        step: 10000,
        unit: 'Kč',
        textProperty: 'Cena nemovitosti'
      },
      loan: {
        min: 200000,
        max: 10000000,
        step: 10000,
        unit: 'Kč',
        textProperty: 'Výše půjčky'
      },
      length: {
        min: 5,
        max: 40,
        step: 1,
        unit: 'let',
        textProperty: 'Délka splácení'
      },
      salary: {
        min: 25000,
        max: 100000,
        step: 1000,
        unit: 'Kč',
        textProperty: ' Čistý měsíční příjem'
      }
    };
  }

  get propertyCostSliderValue(): number {
    return this._propertyCostSliderValue;
  }

  set propertyCostSliderValue(value: number) {
    this._propertyCostSliderValue = value;
    console.log('_propertyCostSliderValue: ' + this._propertyCostSliderValue);
    /*    const out = this.createData();
        for (let i = 0; i < 7; i++) {
          out.datasets[0].data[i] = i * this._firstSliderValue;
          out.datasets[1].data[i] = Math.pow(i * this._firstSliderValue, 1.5);
        }
        this.data = out;*/
  }

  get propertyCostInputValue(): number {
    return this._propertyCostInputValue;
  }

  set propertyCostInputValue(value: number) {
    this._propertyCostInputValue = value;
    console.log('_propertyCostInputValue: ' + this._propertyCostInputValue);
  }

  get loanSliderValue(): number {
    return this._loanSliderValue;
  }

  set loanSliderValue(value: number) {
    this._loanSliderValue = value;
    console.log('_loanSliderValue: ' + this._loanSliderValue);
  }

  get lengthSliderValue(): number {
    return this._lengthSliderValue;
  }

  set lengthSliderValue(value: number) {
    this._lengthSliderValue = value;
    this.observableLength.next(value);
    console.log('_lengthSliderValue: ' + this._lengthSliderValue);
  }

  get salarySliderValue(): number {
    return this._salarySliderValue;
  }

  set salarySliderValue(value: number) {
    this._salarySliderValue = value;
    console.log('_salarySliderValue: ' + this._salarySliderValue);
  }

  get loanInputValue(): number {
    return this._loanInputValue;
  }

  set loanInputValue(value: number) {
    this._loanInputValue = value;
    // if < 3000 : this.radioButtonInV = 3;
    console.log('_loanInputValue: ' + this._loanInputValue);
  }

  get salaryInputValue(): number {
    return this._salaryInputValue;
  }

  set salaryInputValue(value: number) {
    this._salaryInputValue = value;
    console.log('_salaryInputValue: ' + this._salaryInputValue);
  }

  get fixedPeriodInputValue(): number {
    return this._fixedPeriodInputValue;
  }

  set fixedPeriodInputValue(value: number) {
    this._fixedPeriodInputValue = value;
    this.rate = this.setSazba(value);
    console.log('_radioButtonInputValue: ' + this._fixedPeriodInputValue);
  }

  get rate(): number {
    return this._rate;
  }

  set rate(value: number) {
    this._rate = value;
  }

  get isPropertyCostValid(): boolean {
    return this._isPropertyCostValid;
  }

  set isPropertyCostValid(value: boolean) {
    this._isPropertyCostValid = value;
  }

  get isLoanValid(): boolean {
    return this._isLoanValid;
  }

  set isLoanValid(value: boolean) {
    this._isLoanValid = value;
  }

  get islengthValid(): boolean {
    return this._islengthValid;
  }

  set islengthValid(value: boolean) {
    this._islengthValid = value;
  }

  get isSalaryValid(): boolean {
    return this._isSalaryValid;
  }

  set isSalaryValid(value: boolean) {
    this._isSalaryValid = value;
  }

  get isFixedPeriodValid(): boolean {
    return this._isFixedPeriodValid;
  }

  set isFixedPeriodValid(value: boolean) {
    this._isFixedPeriodValid = value;
  }

  get isInstallmentValid(): boolean {
    return this._isInstallmentValid;
  }

  set isInstallmentValid(value: boolean) {
    this._isInstallmentValid = value;
  }

  public setSazba(value: number) {
    if (value === this.firstVal) {
      return this.firstSazba;
    } else if (value === this.secondVal) {
      return this.secondSazba;
    } else if (value === this.thirdVal) {
      return this.thirdSazba;
    } else if (value === this.forthVal) {
      return this.forthSazba;
    }
  }

  private createChartData() {
    return {
      labels: [],
      datasets: [
        {
          data: [1],
          backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ]
        },
      ]
    };
  }

  public updateChartData() {
    const loan = this.loanInputValue !== null ? this.loanInputValue : this.loanSliderValue;
    this.dataChart = {
      labels: ['Výše půjčky (Kč)', 'Výše úroku (Kč)'],
      datasets: [
        {
          data: [loan, this.interestCost],
          backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ]
        },
      ]
    };
  }
}
