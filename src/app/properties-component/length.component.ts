import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {MessageService} from 'primeng/api';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-length-component',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class LengthComponent implements OnInit {
  @Input()
  renderInput: boolean;
  minVal: number;
  maxVal: number;
  valSlider: number;
  stepVal: number;
  unit: string;

  private subscriptionValidate: Subscription;
  /* input */
  textProperty: string;
  valInput: number;

  constructor(public appService: AppService, public messageService: MessageService) {
    this.valSlider = appService.lengthSliderValue;
    this.minVal = appService.data.length.min;
    this.maxVal = appService.data.length.max;
    this.stepVal = appService.data.length.step;
    this.unit = appService.data.length.unit;

    this.textProperty = appService.data.length.textProperty;
    this.subscriptionValidate = this.appService.validateCallSource
      .subscribe(() => {
        appService.islengthValid = this.validate();
      });
  }

  ngOnInit() {
  }

  handleSliderChange(e) {
    // e.value is the new value
    this.appService.lengthSliderValue = e.value;
  }

  handleInputChange(e) {}

  validate() {
    return true;
  }
}
