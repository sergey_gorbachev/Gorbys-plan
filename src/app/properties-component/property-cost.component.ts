import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {Subscription} from 'rxjs';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-property-cost-component',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class PropertyCostComponent implements OnInit {
  @Input()
  renderInput: boolean;
  minVal: number;
  maxVal: number;
  valSlider: number;
  stepVal: number;
  unit: string;

  error: boolean;
  private subscriptionValidate: Subscription;
  /* input */
  textProperty: string;
  valInput: string;

  constructor(public appService: AppService, public messageService: MessageService) {
    this.valSlider = appService.propertyCostSliderValue;
    this.valInput = null;
    this.minVal = appService.data.propertyCost.min;
    this.maxVal = appService.data.propertyCost.max;
    this.stepVal = appService.data.propertyCost.step;
    this.unit = appService.data.propertyCost.unit;

    this.textProperty = appService.data.propertyCost.textProperty;
    this.error = false;
    // this.valInput = 0;
    this.subscriptionValidate = this.appService.validateCallSource
      .subscribe(() => {
        appService.isPropertyCostValid = this.validate();
      });
  }

  ngOnInit() {
  }

  handleSliderChange(e) {
    // e.value is the new value
    this.appService.propertyCostSliderValue = e.value;
  }

  handleInputChange(e) {
    const numValue = +e;
    if (e === '') {
      this.valInput = null;
      this.appService.propertyCostInputValue = null;
    } else if (isNaN(numValue)) {
      // will return an exception in the validation
      this.appService.propertyCostInputValue = 0;
    } else {
      this.appService.propertyCostInputValue = numValue;
    }
  }

  validate() {
    const numValue = +this.valInput;
    if (this.valInput === '') {
      this.error = false;
      return true;
    }
    // will return an exception in the validation
    if (this.valInput !== null && isNaN(numValue)) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Cena nemovitosti',
        detail: 'Vložte číselnou hodnotu do políčka',
        life: 5000
      });
      this.error = true;
      return false;
    } else if (this.valInput !== null && numValue <= 0) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Cena nemovitosti',
        detail: 'Neplatná hodnota',
        life: 5000
      });
      this.error = true;
      return false;
    } else {
      this.error = false;
      return true;
    }
  }
}
