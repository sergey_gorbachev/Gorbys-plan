import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {MessageService} from 'primeng/api';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-salary-component',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class SalaryComponent implements OnInit {
  @Input()
  renderInput: boolean;
  minVal: number;
  maxVal: number;
  valSlider: number;
  stepVal: number;
  unit: string;

  private subscriptionValidate: Subscription;
  /* input */
  textProperty: string;
  valInput: string;
  loanValue: number;

  error: boolean;

  constructor(public appService: AppService, public messageService: MessageService) {
    this.valSlider = appService.salarySliderValue;
    this.valInput = null;
    this.minVal = appService.data.salary.min;
    this.maxVal = appService.data.salary.max;
    this.stepVal = appService.data.salary.step;
    this.unit = appService.data.salary.unit;
    this.error = false;

    this.textProperty = appService.data.salary.textProperty;
    this.subscriptionValidate = this.appService.validateCallSource
      .subscribe(() => {
        this.loanValue = appService.loanInputValue ? appService.loanInputValue : appService.loanSliderValue;
        appService.isSalaryValid = this.validate();
      });
  }

  ngOnInit() {
  }

  handleSliderChange(e) {
    // e.value is the new value
    this.appService.salarySliderValue = e.value;
  }

  handleInputChange(e) {
    const numValue = +e;
    if (e === '') {
      this.valInput = null;
      this.appService.salaryInputValue = null;
    } else if (isNaN(numValue)) {
      // will return an exception in the validation
      this.appService.salaryInputValue = 0;
    } else {
      this.appService.salaryInputValue = numValue;
    }
  }


  validate() {
    const numValue = +this.valInput;
    // will return an exception in the validation
    if (this.valInput !== null && isNaN(numValue)) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše příjmu',
        detail: 'Vložte číselnou hodnotu do políčka',
        life: 5000
      });
      this.error = true;
      return false;
    } else
      // input is less than 0
    if (this.valInput !== null && numValue <= 0) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše příjmu',
        detail: 'Neplatná hodnota',
        life: 5000
      });
      this.error = true;
      return false;
    } else
      // valInput exist if it doesnt exceed 9 annual salaries
    if (this.valInput !== null && numValue * 12 * 9 < this.loanValue) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Riziková hypotéka',
        detail: ' Výše půjčky nesmí přesáhnout 9 ročních příjmů',
        life: 7000
      });
      return false;
    } else
      // valInput doesnt exist and and valSlider exceeds 9 annual salaries
    if (this.valInput == null && this.valSlider * 12 * 9 < this.loanValue) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Riziková hypotéka',
        detail: 'Výše půjčky nesmí přesáhnout 9 ročních příjmů',
        life: 7000
      });
      return false;
    } else {
      this.error = false;
      return true;
    }
  }
}
