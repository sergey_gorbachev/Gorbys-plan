import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {MessageService} from 'primeng/api';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-loan-component',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class LoanComponent implements OnInit {
  @Input()
  renderInput: boolean;
  minVal: number;
  maxVal: number;
  valSlider: number;
  stepVal: number;
  unit: string;

  error: boolean;
  private subscriptionValidate: Subscription;
  /* input */
  textProperty: string;
  valInput: string;

  propertyCostValue: number;

  constructor(public appService: AppService, public messageService: MessageService) {
    this.valSlider = appService.loanSliderValue;
    this.valInput = null;
    this.minVal = appService.data.loan.min;
    this.maxVal = appService.data.loan.max;
    this.stepVal = appService.data.loan.step;
    this.unit = appService.data.loan.unit;

    this.textProperty = appService.data.loan.textProperty;
    this.error = false;
    this.subscriptionValidate = this.appService.validateCallSource
      .subscribe(() => {
        this.propertyCostValue = appService.propertyCostInputValue ? appService.propertyCostInputValue : appService.propertyCostSliderValue;
        appService.isLoanValid = this.validate();
      });
  }

  ngOnInit() {
  }

  handleSliderChange(e) {
    // e.value is the new value
    this.appService.loanSliderValue = e.value;
  }

  handleInputChange(e) {
    const numValue = +e;
    if (e === '') {
      this.valInput = null;
      this.appService.loanInputValue = null;
    } else if (isNaN(numValue)) {
      // will return an exception in the validation
      this.appService.loanInputValue = 0;
    } else {
      this.appService.loanInputValue = numValue;
    }
  }

  validate() {
    const numValue = +this.valInput;
    // will return an exception in the validation
    if (this.valInput !== null && isNaN(numValue)) {
      console.log(this.valInput);
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše půjčky',
        detail: 'Vložte číselnou hodnotu do políčka',
        life: 5000
      });
      this.error = true;
      return false;
    } else
      // input is less than 0
    if (this.valInput !== null && numValue <= 0) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše půjčky',
        detail: 'Neplatná hodnota',
        life: 5000
      });
      this.error = true;
      return false;
    } else
      // valInput exists and greater than 90% from propertyCost
    if (this.valInput && this.propertyCostValue * 0.9 < numValue) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše půjčky',
        detail: 'Nesmí přesáhnout 90 % ceny nemovitosti',
        life: 5000
      });
      return false;
    } else
      // valInput doesnt exist and valSlider greater than 90% from propertyCost
    if (this.valInput == null && this.propertyCostValue * 0.9 < this.valSlider) {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'Výše půjčky',
        detail: 'Nesmí přesáhnout 90 % ceny nemovitosti',
        life: 5000
      });
      return false;
    } else {
      this.error = false;
      return true;
    }
  }
}
