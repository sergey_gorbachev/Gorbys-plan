import { Component, OnInit } from '@angular/core';
import {AppService} from '../services/app.service';

@Component({
  selector: 'app-doughnut',
  templateUrl: './doughnut.component.html',
  styleUrls: ['./doughnut.component.scss']
})
export class DoughnutComponent implements OnInit {

  constructor(public appService: AppService) {
  }

  ngOnInit() {
  }

}
