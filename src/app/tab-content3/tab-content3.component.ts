import {Component, OnInit} from '@angular/core';
import {AppService} from '../services/app.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-tab-content3',
  templateUrl: './tab-content3.component.html',
  styleUrls: ['./tab-content3.component.scss']
})
export class TabContent3Component implements OnInit {

  currencyUnit: string;
  yearUnit: string;
  textProperty: string;
  textLoan: string;
  textLength: string;
  textSalary: string;
  textMortgage: string;

  mortgageCost: number;
  installment: number;
  count: number;

  label1 = 'Výše hypotéky';
  label2 = 'Výše měsíční splátky';
  label3 = 'Počet splátek';

  constructor(public appService: AppService, public messageService: MessageService) {
    this.currencyUnit = 'Kč';
    this.yearUnit = 'let';
    this.textProperty = 'Cena nemovitosti';
    this.textLoan = 'Výše půčky';
    this.textLength = 'Délka splácení';
    this.textSalary = 'Výše splátky';
    this.textMortgage = 'Výsledek';

    this.mortgageCost = null;
    this.installment = null;
    this.count = null;
  }

  ngOnInit() {
  }

  calculate() {
    // recalculate only if validate() will return true. (no errors in components)
    if (this.appService.validate()) {
      this.mortgageCost = this.appService.calculateMortgageCost();
      this.installment = this.appService.calculateInstallment();
      this.count = this.appService.calculateCountOfInstallment();
      this.appService.calculateInterest(this.mortgageCost);
      this.appService.updateChartData();
    }
  }
}
